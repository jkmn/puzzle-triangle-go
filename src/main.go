package main

/*
This version of the code uses caching to ensure that no sub-tree is calculated
more than once.

To execute this code, first build the code:
  go build -o triangle ./src/ver2.go

Then, you can execute it, as follows:
  ./triangle -triangle=triangle.txt
*/

/*
   Copyright (C) 2014 Andrew Roy Jackman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/

import (
	"bufio"
	"flag"
	//	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"strconv"
)

// Flags definition
var cpuprofile = flag.String("cpuprofile", "", "desired filename of CPU profile (none specified disables profiling)")
var trifile = flag.String("triangle", "", "source file of triangle data")

// Data type definition of tree construction.
type Node struct {
	Max    int64
	Value  int64
	Child0 *Node
	Child1 *Node
}

func main() {

	// This code handles profiling options.
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	// Parse trangle data.
	// All computation is done from the topmost node of the tree.
	var node *Node

	if *trifile != "" {
		node = NewTriangleFromFile(*trifile)
	} else {
		log.Println("No input file specified.")
		flag.Usage()
	}

	// Determine the maximum value, and print it.
	var max int64
	if node != nil {
		max = node.FindMax()
	}

	log.Printf("Highest value: %d\n", max)

}

/*
This is the recursive function for determining the highest sum of any
vertical path in the triangle.
*/
func (node *Node) FindMax() int64 {

	// If we already found the maximum for this node and it's children, pass it up the stack.
	if node.Max != -1 {
		return node.Max
	}

	var max0, max1 int64

	if node.Child0 != nil {
		max0 = node.Child0.FindMax()
	} else {
		max0 = 0
	}

	if node.Child1 != nil {
		max1 = node.Child1.FindMax()
	} else {
		max1 = 0
	}

	if max0 > max1 {
		node.Max = node.Value + max0
	} else {
		node.Max = node.Value + max1
	}

	return node.Max
}

/*
This method reads a file of whitespace-seperated integers. If it reads a
non-integer, it'll panic.
*/
func NewTriangleFromFile(filename string) *Node {

	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scnr := bufio.NewScanner(file)
	scnr.Split(bufio.ScanWords)

	// For each integer in the list, create a node.

	nodeList := make([]*Node, 0)
	for scnr.Scan() {
		x, err := strconv.ParseInt(scnr.Text(), 10, 64)
		if err != nil {
			log.Fatal(err)
		}

		node := &Node{
			Max:    -1,
			Value:  x,
			Child0: nil,
			Child1: nil,
		}

		nodeList = append(nodeList, node)
	}

	// Organize the node list into series of slices representing rows.

	listIndex := 0
	rowIndex := 1 // 1-indexed
	rows := make([][]*Node, 0)
	for listIndex < len(nodeList) {
		// Grab the values in the current row.
		// The listIndex tells us where to start the slice.
		// The number of values in the row always coincides with the row number.
		currentRow := nodeList[listIndex : listIndex+rowIndex]

		//		log.Printf("Row length: %d\n", len(currentRow))

		rows = append(rows, currentRow)

		// Advance the listIndex for the next row.
		listIndex += len(currentRow)
		rowIndex++
	}

	//	PrintArray(rows)

	// Now, let's wire up all the nodes with parent/child relationships.

	for row := 0; row < len(rows)-1; row++ {
		//		log.Printf("Row length: %d\n", len(rows[row]))

		for col := 0; col < len(rows[row]); col++ {
			//			log.Printf("Current Row, Col: %d, %d\n", row, col)

			node := rows[row][col]
			node.Child0 = rows[row+1][col]
			node.Child1 = rows[row+1][col+1]
		}
	}

	return rows[0][0]
}

/*
This method represents triangles of single-digit integers of a reasonable size
very nicely. It's good for debugging, but it's a waste of time on large/complex
triangles.
*/
//func PrintArray(arr [][]*Node) {
//	height := len(arr)
//	width := len(arr[height-1]) + 2
//
//	for r, _ := range arr {
//		for i := 0; i < (width/2 - r); i++ {
//			fmt.Print(" ")
//		}
//		for _, node := range arr[r] {
//			fmt.Printf("%d ", node.Value)
//		}
//		fmt.Print("\n")
//	}
//}
